# Currency Converter
## To run this project

1. Clone this repo 
```
git clone https://lordly@bitbucket.org/lordly/lordly-licayan-test.git
```
2. To run the currencyConverter executable jar
```
run the command prompt
cd into the directory
type the following java command:
  syntax:
    java -jar currencyConverter.jar <amountToConvert> <Base Currency> <Target Currency>
  example:
    java -jar currencyConverter.jar 12 EUR USD
```